import sys
from PyQt5 import QtWidgets
from gui import Ui_MainWindow
from ipaddress import IPv4Interface


class LanCalc(QtWidgets.QMainWindow):
    def __init__(self):
        super(LanCalc, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.init_ui()

    def init_ui(self):
        self.setWindowTitle("Калькулятор Сетевика")
        self.ui.IP.setPlaceholderText('ip адрес')
        self.ui.CalcButton.clicked.connect(self.calc)

    def calc(self):
        data = {}
        ipaddr = self.ui.IP.text().replace(',', '.')
        if '/' in ipaddr:
            try:
                data['ipaddr'], prefix = ipaddr.split('/')
            except ValueError:
                return
            try:
                if 0 <= int(prefix) <= 32:
                    self.ui.Prefix.setCurrentIndex(int(prefix))
            except ValueError:
                data['ipaddr'] = 'Value error'
                data['ip_network'] = 'Value error'
                data['ip_broadcast'] = 'Value error'
                data['netmask'] = 'Value error'
                data['wildcard'] = 'Value error'
                data['first_ip'] = 'Value error'
                data['last_ip'] = 'Value error'
                data['len_network'] = 'Value error'
        else:
            data['ipaddr'] = ipaddr
            prefix = str(self.ui.Prefix.currentIndex())
        try:
            interface = IPv4Interface(data['ipaddr'] + '/' + prefix)
        except (TypeError, ValueError):
            data['ipaddr'] = 'Value error'
            data['ip_network'] = 'Value error'
            data['ip_broadcast'] = 'Value error'
            data['netmask'] = 'Value error'
            data['wildcard'] = 'Value error'
            data['first_ip'] = 'Value error'
            data['last_ip'] = 'Value error'
            data['len_network'] = 'Value error'

        try:
            data['ip_network'] = interface.network.network_address.compressed
            data['ip_broadcast'] = interface.network.broadcast_address.compressed
            data['netmask'] = interface.netmask.compressed
            data['wildcard'] = interface.hostmask.compressed
            len_network = int(interface.network.broadcast_address) - int(interface.network.network_address) - 1
            if int(prefix) < 31:
                data['first_ip'] = (interface.network.network_address + 1).compressed
                data['last_ip'] = (interface.network.broadcast_address - 1).compressed
                data['len_network'] = str('{0:,}'.format(len_network).replace(',', ' '))
            elif int(prefix) == 31:
                data['first_ip'] = data['ip_network']
                data['last_ip'] = data['ip_broadcast']
                data['len_network'] = str(2)
            elif int(prefix) == 32:
                data['first_ip'] = data['ipaddr']
                data['last_ip'] = data['ipaddr']
                data['len_network'] = str(1)
        except UnboundLocalError:
            data['ipaddr'] = 'Value error'
            data['ip_network'] = 'Value error'
            data['ip_broadcast'] = 'Value error'
            data['netmask'] = 'Value error'
            data['wildcard'] = 'Value error'
            data['first_ip'] = 'Value error'
            data['last_ip'] = 'Value error'
            data['len_network'] = 'Value error'

        self.ui.ipaddr.setText(data['ipaddr'])
        self.ui.ip_network.setText(data['ip_network'])
        self.ui.ip_broadcast.setText(data['ip_broadcast'])
        self.ui.netmask.setText(data['netmask'])
        self.ui.wildcard.setText(data['wildcard'])
        self.ui.first_ip.setText(data['first_ip'])
        self.ui.last_ip.setText(data['last_ip'])
        self.ui.len_network.setText(data['len_network'])


app = QtWidgets.QApplication([])
application = LanCalc()
application.show()

sys.exit(app.exec())