# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui.ui'
#
# Created by: PyQt5 UI code generator 5.15.9
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


prefix_list = ('0 - 0.0.0.0',
               '1 - 128.0.0.0', '2 - 192.0.0.0', '3 - 224.0.0.0', '4 - 240.0.0.0',
               '5 - 248.0.0.0', '6 - 252.0.0.0', '7 - 254.0.0.0', '8 - 255.0.0.0',
               '9 - 255.128.0.0', '10 - 255.192.0.0', '11 - 255.224.0.0', '12 - 255.240.0.0',
               '13 - 255.248.0.0', '14 - 255.252.0.0', '15 - 255.254.0.0', '16 - 255.255.0.0',
               '17 - 255.255.128.0', '18 - 255.255.192.0', '19 - 255.255.224.0', '20 - 255.255.240.0',
               '21 - 255.255.248.0', '22 - 255.255.252.0', '23 - 255.255.254.0', '24 - 255.255.255.0',
               '25 - 255.255.255.128', '26 - 255.255.255.192', '27 - 255.255.255.224', '28 - 255.255.255.240',
               '29 - 255.255.255.248', '30 - 255.255.255.252', '31 - 255.255.255.254', '32 - 255.255.255.255')


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(360, 560)
        MainWindow.setStyleSheet("QWidget {"
                                 "color: white;"
                                 "background-color: #22222e;"
                                 "}"
                                 "QLineEdit {"
                                 "border: none;"
                                 "}"
                                 "QLineEdit::hover {"
                                 "background-color: #8e8e93;"
                                 "}")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.Label = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.Label.sizePolicy().hasHeightForWidth())
        self.Label.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(18)
        font.setBold(True)
        font.setWeight(75)
        self.Label.setFont(font)
        self.Label.setStyleSheet("color: white")
        self.Label.setAlignment(QtCore.Qt.AlignCenter)
        self.Label.setObjectName("Label")
        self.verticalLayout.addWidget(self.Label)
        self.IP = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.IP.sizePolicy().hasHeightForWidth())
        self.IP.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(18)
        font.setBold(True)
        font.setWeight(75)
        self.IP.setFont(font)
        self.IP.setStyleSheet("background-color: #8e8e93;"
                              "border-radius: 8;"
                              "color: white")
        self.IP.setMaxLength(18)
        self.IP.setAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.IP.setObjectName("IP")
        self.verticalLayout.addWidget(self.IP)
        self.Prefix = QtWidgets.QComboBox(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.Prefix.sizePolicy().hasHeightForWidth())
        self.Prefix.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.Prefix.setFont(font)
        self.Prefix.setStyleSheet("background-color: #8e8e93;"
                                  "border-radius: 8;"
                                  "color: white")
        self.Prefix.setObjectName("Prefix")
        self.verticalLayout.addWidget(self.Prefix)
        self.CalcButton = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.CalcButton.sizePolicy().hasHeightForWidth())
        self.CalcButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.CalcButton.setFont(font)
        self.CalcButton.setStyleSheet("QPushButton  {"
                                      "background-color: #4cd964;"
                                      "border-radius: 8;"
                                      "color: white"
                                      "}"
                                      "QPushButton:pressed {"
                                      "background-color: #8e8e93"
                                      "}")
        self.CalcButton.setObjectName("CalcButton")
        self.verticalLayout.addWidget(self.CalcButton)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.k_ip_network = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.k_ip_network.sizePolicy().hasHeightForWidth())
        self.k_ip_network.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.k_ip_network.setFont(font)
        self.k_ip_network.setReadOnly(True)
        self.k_ip_network.setObjectName("k_ip_network")
        self.gridLayout.addWidget(self.k_ip_network, 4, 0, 1, 1)
        self.k_len_network = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.k_len_network.sizePolicy().hasHeightForWidth())
        self.k_len_network.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.k_len_network.setFont(font)
        self.k_len_network.setReadOnly(True)
        self.k_len_network.setObjectName("k_len_network")
        self.gridLayout.addWidget(self.k_len_network, 6, 0, 1, 1)
        self.k_ip_broadcast = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.k_ip_broadcast.sizePolicy().hasHeightForWidth())
        self.k_ip_broadcast.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.k_ip_broadcast.setFont(font)
        self.k_ip_broadcast.setReadOnly(True)
        self.k_ip_broadcast.setObjectName("k_ip_broadcast")
        self.gridLayout.addWidget(self.k_ip_broadcast, 5, 0, 1, 1)
        self.k_wildcard = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.k_wildcard.sizePolicy().hasHeightForWidth())
        self.k_wildcard.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.k_wildcard.setFont(font)
        self.k_wildcard.setReadOnly(True)
        self.k_wildcard.setObjectName("k_wildcard")
        self.gridLayout.addWidget(self.k_wildcard, 3, 0, 1, 1)
        self.k_first_ip = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.k_first_ip.sizePolicy().hasHeightForWidth())
        self.k_first_ip.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.k_first_ip.setFont(font)
        self.k_first_ip.setReadOnly(True)
        self.k_first_ip.setObjectName("k_first_ip")
        self.gridLayout.addWidget(self.k_first_ip, 7, 0, 1, 1)
        self.k_last_ip = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.k_last_ip.sizePolicy().hasHeightForWidth())
        self.k_last_ip.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.k_last_ip.setFont(font)
        self.k_last_ip.setReadOnly(True)
        self.k_last_ip.setObjectName("k_last_ip")
        self.gridLayout.addWidget(self.k_last_ip, 8, 0, 1, 1)
        self.k_ipaddr = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.k_ipaddr.sizePolicy().hasHeightForWidth())
        self.k_ipaddr.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.k_ipaddr.setFont(font)
        self.k_ipaddr.setReadOnly(True)
        self.k_ipaddr.setObjectName("k_ipaddr")
        self.gridLayout.addWidget(self.k_ipaddr, 1, 0, 1, 1)
        self.k_netmask = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.k_netmask.sizePolicy().hasHeightForWidth())
        self.k_netmask.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.k_netmask.setFont(font)
        self.k_netmask.setReadOnly(True)
        self.k_netmask.setObjectName("k_netmask")
        self.gridLayout.addWidget(self.k_netmask, 2, 0, 1, 1)
        self.last_ip = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.last_ip.sizePolicy().hasHeightForWidth())
        self.last_ip.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.last_ip.setFont(font)
        self.last_ip.setReadOnly(True)
        self.last_ip.setObjectName("last_ip")
        self.gridLayout.addWidget(self.last_ip, 8, 1, 1, 1)
        self.first_ip = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.first_ip.sizePolicy().hasHeightForWidth())
        self.first_ip.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.first_ip.setFont(font)
        self.first_ip.setReadOnly(True)
        self.first_ip.setObjectName("first_ip")
        self.gridLayout.addWidget(self.first_ip, 7, 1, 1, 1)
        self.len_network = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.len_network.sizePolicy().hasHeightForWidth())
        self.len_network.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.len_network.setFont(font)
        self.len_network.setReadOnly(True)
        self.len_network.setObjectName("len_network")
        self.gridLayout.addWidget(self.len_network, 6, 1, 1, 1)
        self.ip_broadcast = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.ip_broadcast.sizePolicy().hasHeightForWidth())
        self.ip_broadcast.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.ip_broadcast.setFont(font)
        self.ip_broadcast.setReadOnly(True)
        self.ip_broadcast.setObjectName("ip_broadcast")
        self.gridLayout.addWidget(self.ip_broadcast, 5, 1, 1, 1)
        self.ip_network = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.ip_network.sizePolicy().hasHeightForWidth())
        self.ip_network.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.ip_network.setFont(font)
        self.ip_network.setReadOnly(True)
        self.ip_network.setObjectName("ip_network")
        self.gridLayout.addWidget(self.ip_network, 4, 1, 1, 1)
        self.wildcard = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.wildcard.sizePolicy().hasHeightForWidth())
        self.wildcard.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.wildcard.setFont(font)
        self.wildcard.setReadOnly(True)
        self.wildcard.setObjectName("wildcard")
        self.gridLayout.addWidget(self.wildcard, 3, 1, 1, 1)
        self.netmask = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.netmask.sizePolicy().hasHeightForWidth())
        self.netmask.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.netmask.setFont(font)
        self.netmask.setReadOnly(True)
        self.netmask.setObjectName("netmask")
        self.gridLayout.addWidget(self.netmask, 2, 1, 1, 1)
        self.ipaddr = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.ipaddr.sizePolicy().hasHeightForWidth())
        self.ipaddr.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Noto Sans Thin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.ipaddr.setFont(font)
        self.ipaddr.setReadOnly(True)
        self.ipaddr.setObjectName("ipaddr")
        self.gridLayout.addWidget(self.ipaddr, 1, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        # Add items to prefix
        self.Prefix.addItems(prefix_list)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "LanCalc"))
        self.Label.setText(_translate("MainWindow", "Lan Calculator"))
        self.CalcButton.setText(_translate("MainWindow", "Calculate"))
        self.k_ip_network.setText(_translate("MainWindow", "Network IP address"))
        self.k_len_network.setText(_translate("MainWindow", "Hosts count"))
        self.k_ip_broadcast.setText(_translate("MainWindow", "Broadcast IP address"))
        self.k_wildcard.setText(_translate("MainWindow", "Wildcard mask"))
        self.k_first_ip.setText(_translate("MainWindow", "First IP address"))
        self.k_last_ip.setText(_translate("MainWindow", "Last IP address"))
        self.k_ipaddr.setText(_translate("MainWindow", "IP address"))
        self.k_netmask.setText(_translate("MainWindow", "Network mask"))


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
